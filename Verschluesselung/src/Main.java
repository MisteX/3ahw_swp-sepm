import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.Base64.Decoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Main {

	public static void main(String[] args) 
	{
		String pw="ich";
		String codedText;
		String cryptedText;		
		try
		{
//			// Das Password bzw der Schluesseltext
//			String keyStr = "geheim";
//			// byte-Array erzeugen
//			byte[] key = keyStr.getBytes("UTF-8");
//			// aus dem Array einen Hash-Wert erzeugen mit MD5 oder SHA
//			MessageDigest sha = MessageDigest.getInstance("SHA-256");
//			key = sha.digest(key);
//			// nur die ersten 128 Bits verwenden
//			key = Arrays.copyOf(key,16);
//			// der fertige Schluessel
//			SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
//
//			//der zu verschlüsselnde Text
//			String text="Die HTL ist super";
//
//			// verschluesseln
//			Cipher cipher = Cipher.getInstance("AES");
//			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
//			byte[] encrypted = cipher.doFinal(text.getBytes());
//
//			//bytes zu Base64.String konvertieren (dient der Lesbarkeit)
//			Encoder myEncoder = Base64.getEncoder();
//			byte[] geheim = cryptedText.getBytes();
//
//			//Ergebnis
//			System.out.println(geheim);
//			
//			byte[] keyBytes = keyStr.getBytes("UTF-8");
//			sha = MessageDigest.getInstance("SHA-256");
//			keyBytes = sha.digest(keyBytes);
//			keyBytes = Arrays.copyOf(keyBytes,16);
//			secretKeySpec = new SecretKeySpec(keyBytes, "AES");
//		
//  			Decoder myDecoder = Base64.getDecoder();
//  			byte[] crypted2 = myDecoder.decode(geheim);
//
//  			Cipher cipher2 = Cipher.getInstance("AES");
//  			cipher2.init(Cipher.DECRYPT_MODE, secretKeySpec);
//  			byte[] cipherData2 = cipher2.doFinal(crypted2);
//  			String erg = new String(cipherData2);
//
//  			System.out.println(erg);


			codedText = encode("die HTL ist super", pw);
			decode(codedText, pw);
		}
		catch (Exception except)
		{
			System.out.println(except.getClass().getName()+ " " + except.getMessage());
		}
	}
	public static String encode (String text, String key) throws Exception
	{
		byte[] keyBytes = key.getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		keyBytes = sha.digest(keyBytes);
		keyBytes = Arrays.copyOf(keyBytes,16);
		SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		byte[] encrypted = cipher.doFinal(text.getBytes());

		Encoder myEncoder = Base64.getEncoder();
		byte[] geheim = myEncoder.encode(encrypted);
		
		String cryptedText = new String(geheim);
		System.out.println(cryptedText);
		return cryptedText;
	}
	
	
	public static void decode(String cryptedText, String key) throws Exception
	{
		byte[] keyBytes = key.getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		keyBytes = sha.digest(keyBytes);
		keyBytes = Arrays.copyOf(keyBytes,16);
		SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");
		
  		Decoder myDecoder = Base64.getDecoder();
  		byte[] crypted2 = myDecoder.decode(cryptedText);

  		Cipher cipher2 = Cipher.getInstance("AES");
  		cipher2.init(Cipher.DECRYPT_MODE, secretKeySpec);
  		byte[] cipherData2 = cipher2.doFinal(crypted2);
  		String erg = new String(cipherData2);

  		System.out.println(erg);
	}


}
