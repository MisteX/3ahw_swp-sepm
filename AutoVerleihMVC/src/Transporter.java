import java.io.Serializable;

public class Transporter extends Fahrzeug implements Serializable
{
	private int sitzplaetze;
	private int laderaum;

	public Transporter()
	{
	}
	public Transporter (int bj, String hst, String kzeichen, String name, int sp, int lr)
	{
		super(bj, hst, kzeichen, "Transporter", name);
		this.laderaum = lr;
		this.sitzplaetze = sp;
	}
	
	public int getSitzplaetze() 
	{
		return sitzplaetze;
	}
	
	public int getLaderaum() 
	{
		return laderaum;
	}
	public void setSitzplaetze(int sitzplaetze) 
	{
		this.sitzplaetze = sitzplaetze;
	}

	public void setLaderaum(int laderaum) 
	{
		this.laderaum = laderaum;
	}
}
