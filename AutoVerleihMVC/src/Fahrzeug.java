import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;

public class Fahrzeug implements Serializable
{
	private int baujahr;
	private String hersteller;
	private String kennzeichen;
	private String fahrzeugtyp;
	private boolean vorhandenheit = true;
	private String name;
	XMLEncoder enc;

	public Fahrzeug()
	{
	}
	public Fahrzeug (int bj, String hs, String kzeichen, String ft, String name)
	{
		this.baujahr = bj;
		this.hersteller = hs;
		this.kennzeichen = kzeichen;
		this.fahrzeugtyp = ft;
		this.name = name;
	}
	
	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	public void setHersteller(String hersteller) {
		this.hersteller = hersteller;
	}

	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}

	public void setFahrzeugtyp(String fahrzeugtyp) {
		this.fahrzeugtyp = fahrzeugtyp;
	}

	public void setVorhandenheit(boolean vorhandenheit) {
		this.vorhandenheit = vorhandenheit;
	}

	public void setName(String name) {
		this.name = name;
	}
	public int getBaujahr() 
	{
		return baujahr;
	}
	public String getHersteller() 
	{
		return hersteller;
	}
	public String getKennzeichen() 
	{
		return kennzeichen;
	}
	public String getFahrzeugtyp() 
	{
		return fahrzeugtyp;
	}
	public boolean getVorhandenheit()
	{
		return vorhandenheit;
	}
	public void setStatus(boolean vorhanden)
	{
		this.vorhandenheit = vorhanden;
	}
	public String getName()
	{
		return this.name;
	}
}
