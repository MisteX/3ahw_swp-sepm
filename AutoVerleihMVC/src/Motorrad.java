import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.Serializable;

public class Motorrad extends Fahrzeug implements Serializable
{
	private int hubraum;

	public Motorrad (int bj, String hst, String kzeichen,String name, int hr)
	{
		super(bj, hst, kzeichen, "Motorrad", name);
		this.hubraum = hr;
	}
	public Motorrad()
	{
	}

	public int getHubraum() 
	{
		return hubraum;
	}
	public void setHubraum(int hubraum) 
	{
		this.hubraum = hubraum;
	}
}
