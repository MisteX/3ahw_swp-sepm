import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;

public class GUI extends JFrame
{	
	private Verleih verl = new Verleih();
	Fahrzeug fahrzeug;
	
	public GUI()
	{
		JPanel panel = new JPanel();
		
		for(int i=0; i<verl.getGesamtBestand() ; i++)
		{
			fahrzeug = verl.getFahrzeuge(i);
			panel.setLayout(new GridLayout(7,2));
			
		}
		for(int i=0;i<verl.getGesamtBestand();i++)
		{
			JButton button = new JButton();
			button.setText(verl.getFahrzeuge(i).getName());
			button.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent arg0) 
				{
				//	System.out.println(arg0.getActionCommand());
					fahrzeug=verl.getFahrzeugByName(arg0.getActionCommand());
					
					if(!fahrzeug.getVorhandenheit())
					{
						verl.zurueckgeben(fahrzeug.getFahrzeugtyp(), fahrzeug.getName());
						button.setBackground(Color.BLUE);
						button.setForeground(Color.WHITE);
					}
					else 
					{
						verl.ausleihen(fahrzeug.getFahrzeugtyp(), fahrzeug.getName());
						button.setBackground(Color.RED);
						button.setForeground(Color.BLACK);
					}
				}
			});
			if(!verl.getFahrzeuge(i).getVorhandenheit())
			{
				button.setBackground(Color.RED);
				button.setForeground(Color.BLACK);
			}
			else 
			{
				button.setBackground(Color.BLUE);
				button.setForeground(Color.WHITE);
			}
			
			panel.add(button);
			
		}
			
		this.add(panel);
		this.setSize(new Dimension(1000,750));
		this.setVisible(true);
	}
/*
	public int Randomizer()
	{
		int rand = (int) Math.random();
		return rand;
	}*/
}

