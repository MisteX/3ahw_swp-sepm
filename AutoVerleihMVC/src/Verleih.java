import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.ArrayList;

public class Verleih  implements Serializable
{
	private ArrayList<Fahrzeug> alleFahrzeuge= new ArrayList<Fahrzeug>();

	Fahrzeug passat = new PKW(1999, "Volkswagen", "BP-5637ZU", "Passat", "Klimaanlage");
	Fahrzeug insignia = new PKW(2005, "Opel", "BP-9463IO", "Insignia", "Tempomat");
	Fahrzeug civic = new PKW(1995, "Honda", "BP-78954P", "Civic", "CD-Radio");
	Fahrzeug rav4 = new PKW(1997, "Toyota", "BP-4832HS", "Rav4", "hohes Fahrwerk");
	Fahrzeug evo6 = new PKW(1999, "Mitsubishi", "BP-9685G", "Evo.VI", "Lautsprechanlage");

	Fahrzeug s3000rr = new Motorrad(2004, "BMW", "BP-563GR", "S3000rr", 250); 
	Fahrzeug repsol = new Motorrad(2002, "Honda", "BP-934SU", "Repsol", 150);
	Fahrzeug series1000 = new Motorrad(2010, "Yamaha", "BP-894UI", "Series1000", 500);
	Fahrzeug jans = new Motorrad(2000, "KTM", "BP-023HD", "Jans",350);
	Fahrzeug hablung = new Motorrad(1995, "BMW", "BP-466UI", "Hablung", 150);
	Fahrzeug banel = new Motorrad(2016, "Yamaha", "BP-465FG", "Banel", 650);
	Fahrzeug franger = new Motorrad(1989, "Honda", "BP-641CF", "Franger", 400);

	Fahrzeug sprinter = new Transporter(1999,"Volkswagen","BP-5492DH", "Sprinter", 7, 800);
	Fahrzeug dokker = new Transporter(1999,"Dacia","BP-6546GD", "Dokker", 3, 1500);
	
	XMLEncoder enc;
	XMLDecoder dec;
		
	public Verleih()

	{

		alleFahrzeuge.add(passat);
		alleFahrzeuge.add(insignia);
		alleFahrzeuge.add(civic);
		alleFahrzeuge.add(rav4);
		alleFahrzeuge.add(evo6);

		alleFahrzeuge.add(s3000rr);
		alleFahrzeuge.add(repsol);
		alleFahrzeuge.add(series1000);
		alleFahrzeuge.add(jans);
		alleFahrzeuge.add(hablung);
		alleFahrzeuge.add(banel);
		alleFahrzeuge.add(franger);

		alleFahrzeuge.add(sprinter);
		alleFahrzeuge.add(dokker);
	}

	public void ausleihen (String typ, String name)
	{
		if(typ == "PKW")
		{
			switch(name)
			{
			case "Passat": if(passat.getVorhandenheit()){ passat.setStatus(false); System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Insignia": if(insignia.getVorhandenheit()){ insignia.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Civic": if(civic.getVorhandenheit()){ civic.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Rav4": if(rav4.getVorhandenheit()){ rav4.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Evo.VI": if(rav4.getVorhandenheit()){ evo6.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			}
		}
		if(typ == "Motorrad")
		{
			switch(name)
			{
			case "S3000rr": if(s3000rr.getVorhandenheit()){ s3000rr.setStatus(false); System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Repsol": if(repsol.getVorhandenheit()){ repsol.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Series1000": if(series1000.getVorhandenheit()){ series1000.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Jans": if(jans.getVorhandenheit()){ jans.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Hablung": if(hablung.getVorhandenheit()){ hablung.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Banel": if(banel.getVorhandenheit()){ banel.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Franger": if(franger.getVorhandenheit()){ franger.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;

			}
		}
		if(typ == "Transporter")
		{
			switch(name)
			{
			case "Sprinter": if(sprinter.getVorhandenheit()){ sprinter.setStatus(false); System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			case "Dokker": if(dokker.getVorhandenheit()){ dokker.setStatus(false);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			}

		}
	}
	public void zurueckgeben(String typ, String name)
	{
		if(typ == "PKW")
		{
			switch(name)
			{
			case "Passat": if(!passat.getVorhandenheit()){passat.setStatus(true); System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "Insignia": if(!insignia.getVorhandenheit()){insignia.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "Civic": if(!civic.getVorhandenheit()){civic.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "Rav4": if(!rav4.getVorhandenheit()){rav4.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "evo6": if(rav4.getVorhandenheit()){ evo6.setStatus(true);System.out.println("Fahrzeug wurde ausgeliehen."); }
					else System.out.println("Fahrzeug derzeit nicht vorhanden!"); break;
			}
		}
		if(typ == "Motorrad")
		{
			switch(name)
			{
			case "S3000rr": if(!s3000rr.getVorhandenheit()){s3000rr.setStatus(true); System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "Repsol": if(!repsol.getVorhandenheit()){repsol.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "Series1000": if(!series1000.getVorhandenheit()){series1000.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "Jans": if(!jans.getVorhandenheit()){jans.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "Hablung": if(!hablung.getVorhandenheit()){hablung.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "Banel": if(!banel.getVorhandenheit()){banel.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			case "Franger": if(!franger.getVorhandenheit()){franger.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;

			}
		}
		if(typ == "Transporter")
		{
			switch(name)
			{
			case "Sprinter": if(!sprinter.getVorhandenheit()){sprinter.setStatus(true); System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug nicht vorhanden!"); break;
			case "Dokker": if(!dokker.getVorhandenheit()){dokker.setStatus(true);System.out.println("Rueckgabe erfolgreich!");}
					else System.out.println("Fahrzeug wurde nicht ausgeliehen"); break;
			}

		}
	}
	public void ausgabeVorhandeneFahrzeuge()
	{
		for(int i=0; i<alleFahrzeuge.size(); i++)
		{
			if(alleFahrzeuge.get(i).getVorhandenheit())
			{
				System.out.println(alleFahrzeuge.get(i).getName());
			}
		}
	}
	public Fahrzeug getFahrzeuge(int fnr)
	{
		return alleFahrzeuge.get(fnr);
	}
	
	public int getGesamtBestand()
	{
		return alleFahrzeuge.size();
	}
	
	public Fahrzeug getFahrzeugByName(String name)
	{
		Fahrzeug fahrzeug = null;
		for(int i=0; i<alleFahrzeuge.size();i++)
		{
			String fahrname=alleFahrzeuge.get(i).getName();
			if(fahrname.equals(name))
			{
				fahrzeug=alleFahrzeuge.get(i);
				break;
			}
		}
		return fahrzeug;
	}
	public ArrayList<Fahrzeug> getAlleFahrzeuge() 
	{
		return alleFahrzeuge;
	}

	public void setAlleFahrzeuge(ArrayList<Fahrzeug> alleFahrzeuge) 
	{
		this.alleFahrzeuge = alleFahrzeuge;
	}
	
	public void saveAllUsingXML()
	{
		try 
		{
			enc = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("E:/Schule/XMLProjekte/polizeipark.xml")));
			enc.writeObject(alleFahrzeuge);
			enc.close();
		} 
		catch (Exception e) 
		{
			System.err.println(e.getMessage());
		}
	}
	public void getEverythingBackUsingXML()
	{
		try
		{
			dec= new XMLDecoder(new BufferedInputStream(new FileInputStream("E:/Schule/XMLProjekte/polizeipark.xml")));
			alleFahrzeuge = (ArrayList<Fahrzeug>) dec.readObject();
			System.out.println("Alle Fahrzeuge geladen!");
			System.out.println(alleFahrzeuge);
		}
		catch (Exception exc)
		{
			System.err.println(exc.getMessage());
		}
	}
}
