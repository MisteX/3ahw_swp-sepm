import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.Serializable;

public class PKW extends Fahrzeug implements Serializable
{
	private String besonderheit;

	public PKW (int bj, String hst, String kzeichen, String name, String besonders)
	{
		super(bj, hst, kzeichen, "PKW", name);
		this.besonderheit = besonders;
	}
	public PKW()
	{
	}

	public String getBesonderheit() 
	{
		return besonderheit;
	}
	public void setBesonderheit(String besonderheit) 
	{
		this.besonderheit = besonderheit;
	}
}

